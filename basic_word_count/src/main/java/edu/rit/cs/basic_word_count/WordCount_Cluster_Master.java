package edu.rit.cs.basic_word_count;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;


/**
 * @author Jasmine Liang
 * @email: yxl5521@rit.edu
 * @date: 2019-09-10
 */
//whole process takes about 9s
public class WordCount_Cluster_Master {
    public static final String AMAZON_FINE_FOOD_REVIEWS_file = "amazon-fine-food-reviews/Reviews.csv";

    private static final int PORT = 7897;

    /**
     * parse the file into a list of AmazonFineFoodReview(AFFR) objects
     * divided into partitions
     *
     * @param dataset_file the data set to read
     * @return the list of AFFR objects
     */
    public static List<List<AmazonFineFoodReview>> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> reviews = new ArrayList<>();
        List<List<AmazonFineFoodReview>> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))) {
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                AmazonFineFoodReview review = new AmazonFineFoodReview(reviewLine);
                reviews.add(review);
                //store at most 150000 AmazonFineFoodReview in each partitions
                //separate the file to about 4 partitions
                if (reviews.size() % 150000 == 0) {
                    allReviews.add(new ArrayList<>(reviews));
                    reviews.clear();
                }
            }
            allReviews.add(reviews);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    /**
     * merge all the sorted partitions
     *
     * @param old     the previous map to get the data from
     * @param current the new map to merge
     */
    public static void mergeMap(Map<String, Integer> old, Map<String, Integer> current) {
        if (current.isEmpty()) {
            current.putAll(old);
        } else {
            old.forEach((k, v) -> {
                if (current.containsKey(k)) {
                    current.put(k, v + current.get(k));
                } else {
                    current.put(k, v);
                }
            });
        }
    }

    /**
     * print the word count list/map
     *
     * @param wordcount
     */
    public static void print_word_count(Map<String, Integer> wordcount) {
        for (String word : wordcount.keySet()) {
            System.out.println(word + " : " + wordcount.get(word));
        }
    }

    //inner thread class as a client, to pass partitions to workers
    static class MasterClient extends Thread {
        List<AmazonFineFoodReview> partition = new ArrayList<>();
        String ip;
        ObjectOutputStream out;
        Socket socket;

        public MasterClient(String ip, List<AmazonFineFoodReview> partition) {
            this.partition = partition;
            this.ip = ip;
            try {
                socket = new Socket(ip, 7896);
                this.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run() {
            try {
                out = new ObjectOutputStream(socket.getOutputStream());
                out.writeObject(partition);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //inner thraed class as a server, received the final sorted partitions and merge
    static class MasterServer implements Callable<Map<String, Integer>> {
        int size;
        ObjectInputStream in;
        ServerSocket serverSocket;
        Map<String, Integer> finalWordCount;
        Socket clientSocket;

        public MasterServer(int size, Map<String, Integer> finalWordCount) {
            this.size = size;
            this.finalWordCount = finalWordCount;
            try {
                serverSocket = new ServerSocket(PORT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public Map<String, Integer> call() throws Exception {
            try {
                int clients = size;
                //know the amount of clients and loop that amount of times
                while (clients-- > 0) {
                    clientSocket = serverSocket.accept();
                    in = new ObjectInputStream(clientSocket.getInputStream());
                    //merge immediately when get a new partition
                    mergeMap((Map<String, Integer>) in.readObject(), finalWordCount);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                clientSocket.close();
            }
            return finalWordCount;
        }
    }

    public static void main(String[] args) {
        List<List<AmazonFineFoodReview>> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file);
        List<String> ips = new ArrayList<>();
        Map<String, Integer> finalWordCount = new TreeMap<>();

        for (int j = 0; j < allReviews.size(); j++) {
            boolean flag = true;
            //get the ip, use while loop and flag to make sure we are using different "machines"
            while (flag) {
                Scanner ipobj = new Scanner(System.in);
                System.out.println("Enter a Worker Node's IP Address:");
                String ip = ipobj.nextLine();
                if (!ips.contains(ip)) {
                    ips.add(ip);
                    flag = false;
                } else {
                    System.out.println("This Node has already being used, please enter a new IP");
                }
            }
        }

        MyTimer myTimer = new MyTimer("wordCount & tokenize");
        myTimer.start_timer();
        //send partitions to each workers
        for (int i = 0; i < allReviews.size(); i++) {
            new MasterClient(ips.get(i), allReviews.get(i));
        }

        //ready to receive the sorted partitions
        FutureTask<Map<String, Integer>> serverTask = new FutureTask<>(new MasterServer(allReviews.size(), finalWordCount));
        new Thread(serverTask).start();
        myTimer.stop_timer();

        try {
            //get the final merged list
            finalWordCount = serverTask.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        print_word_count(finalWordCount);

        myTimer.print_elapsed_time();

    }
}
