package edu.rit.cs.basic_word_count;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Jasmine Liang
 * @email: yxl5521@rit.edu
 * @date: 2019-09-11
 */
//the workers nodes, need master node's ip address
public class WordCount_Cluster_Workers {

    private static final int PORT = 7896;

    /**
     * Emit 1 for every word and store this as a <key, value> pair
     *
     * @param partitionReviews reviews that being separated already
     * @return the list of kv pairs
     */
    public static List<KV<String, Integer>> tokenizer(List<AmazonFineFoodReview> partitionReviews) {
        List<KV<String, Integer>> kv_pairs = new ArrayList<KV<String, Integer>>();

        for (AmazonFineFoodReview review : partitionReviews) {
            Pattern pattern = Pattern.compile("([a-zA-Z]+)");
            Matcher matcher = pattern.matcher(review.get_Summary());

            while (matcher.find())
                kv_pairs.add(new KV(matcher.group().toLowerCase(), 1));
        }
        return kv_pairs;
    }


    /**
     * count the frequency of each unique word
     *
     * @param words list of the words to count(store in KV pair)
     * @return a map of words with their count, sorted
     */
    public static Map<String, Integer> wordCount(List<KV<String, Integer>> words) {
        Map<String, Integer> results = new HashMap<>();

        for (KV<String, Integer> kv : words) {
            if (!results.containsKey(kv.getKey())) {
                results.put(kv.getKey(), kv.getValue());
            } else {
                int init_value = results.get(kv.getKey());
                results.replace(kv.getKey(), init_value, init_value + kv.getValue());
            }
        }
        return new TreeMap<>(results);
    }

    //inner thread class as a server to get data from master
    static class WorkerServer extends Thread {
        ServerSocket serverSocket;
        Socket clientSocket;
        ObjectInputStream in;
        String ip;

        public WorkerServer(String ip) {
            try {
                this.ip = ip;
                serverSocket = new ServerSocket(PORT);
                this.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run() {
            try {
                clientSocket = serverSocket.accept();
                in = new ObjectInputStream(clientSocket.getInputStream());
                Object partitionList = in.readObject();
                if (partitionList instanceof List) {
                    List<KV<String, Integer>> tokenize = tokenizer((List<AmazonFineFoodReview>) partitionList);
                    //call the client to pass back the data
                    new WorkerClient(wordCount(tokenize), ip);
                } else {
                    System.out.println("Error in worker server");
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //inner thread class as a client, pass back the sorted/counted/tokenized words
    static class WorkerClient extends Thread {
        Socket socket = null;
        Map<String, Integer> sortedPartition = new TreeMap<>();
        ObjectOutputStream out;
        String ip;

        public WorkerClient(Map<String, Integer> sortedPartition, String ip) {

            this.ip = ip;
            this.sortedPartition.putAll(sortedPartition);
            this.start();
        }

        public void run() {
            try {
                socket = new Socket(ip, 7897);
                out = new ObjectOutputStream(socket.getOutputStream());
                out.writeObject(sortedPartition);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner ip = new Scanner(System.in);
        System.out.println("Enter Master Node's IP Address:");
        new WorkerServer(ip.nextLine());
    }
}
