package edu.rit.cs.basic_word_count;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//sort the WordCount, takes about 1.225s in total
public class WordCount_Seq_Sorted {
    public static final String AMAZON_FINE_FOOD_REVIEWS_file = "amazon-fine-food-reviews/Reviews.csv";

    /**
     * parse the file into a list of AmazonFineFoodReview(AFFR) objects
     *
     * @param dataset_file the data set to read
     * @return the list of AFFR objects
     */
    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))) {
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    /**
     * print the word count list/map
     *
     * @param wordcount
     */
    public static void print_word_count(Map<String, Integer> wordcount) {
        for (String word : wordcount.keySet()) {
            System.out.println(word + " : " + wordcount.get(word));
        }
    }

    /**
     * Emit 1 for every word and store this as a <key, value> pair
     *
     * @param allReviews
     * @return
     */
    public static List<KV<String, Integer>> map(List<AmazonFineFoodReview> allReviews) {
        List<KV<String, Integer>> kv_pairs = new ArrayList<KV<String, Integer>>();

        for (AmazonFineFoodReview review : allReviews) {
            Pattern pattern = Pattern.compile("([a-zA-Z]+)");
            Matcher matcher = pattern.matcher(review.get_Summary());

            while (matcher.find())
                kv_pairs.add(new KV(matcher.group().toLowerCase(), 1));
        }
        return kv_pairs;
    }


    /**
     * count the frequency of each unique word
     *
     * @param kv_pairs
     * @return a list of words with their count
     */
    public static Map<String, Integer> reduce(List<KV<String, Integer>> kv_pairs) {
        Map<String, Integer> results = new HashMap<>();

        for (KV<String, Integer> kv : kv_pairs) {
            if (!results.containsKey(kv.getKey())) {
                results.put(kv.getKey(), kv.getValue());
            } else {
                int init_value = results.get(kv.getKey());
                results.replace(kv.getKey(), init_value, init_value + kv.getValue());
            }
        }
        //return tree map to sort automatically
        return new TreeMap<>(results);
    }

    public static void main(String[] args) {
        List<AmazonFineFoodReview> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file);

        System.out.println("Finished reading all reviews, now performing word count...");

        //start the timer for tokenize the words
        MyTimer myMapTimer = new MyTimer("tokenize");
        myMapTimer.start_timer();
        List<KV<String, Integer>> kv_pairs = map(allReviews);
        myMapTimer.stop_timer();

        //start the timer for counting the words
        MyTimer myTimer = new MyTimer("wordCount");
        myTimer.start_timer();
        Map<String, Integer> results = reduce(kv_pairs);
        myTimer.stop_timer();

        print_word_count(results);

        myMapTimer.print_elapsed_time();
        myTimer.print_elapsed_time();
    }

}
