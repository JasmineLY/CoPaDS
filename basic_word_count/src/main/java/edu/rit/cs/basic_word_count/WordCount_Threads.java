package edu.rit.cs.basic_word_count;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Jasmine Liang
 * @email: yxl5521@rit.edu
 * @date: 2019-09-09
 */
//count and tokenize the words use threads, takes about 0.6s
public class WordCount_Threads {
    public static final String AMAZON_FINE_FOOD_REVIEWS_file = "amazon-fine-food-reviews/Reviews.csv";

    /**
     * parse the file into a list of AmazonFineFoodReview(AFFR) objects
     * separate into patitions
     *
     * @param dataset_file the data set to read
     * @return the list of AFFR objects
     */
    public static List<List<AmazonFineFoodReview>> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> reviews = new ArrayList<>();
        List<List<AmazonFineFoodReview>> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))) {
            String reviewLine = null;
            // read the header line
            br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                AmazonFineFoodReview review = new AmazonFineFoodReview(reviewLine);
                reviews.add(review);
                //store at most 150000 AmazonFineFoodReview in each partitions
                //separate the file to about 4 partitions
                if (reviews.size() % 150000 == 0) {
                    allReviews.add(new ArrayList<>(reviews));
                    reviews.clear();
                }
            }
            allReviews.add(reviews);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    /**
     * the inner thread class to Emit 1 for every word and store this as a <key, value> pair
     * tokenize the data
     */
    private static class Tokenizer implements Callable<List<KV<String, Integer>>> {

        private List<AmazonFineFoodReview> partitionReviews;

        public Tokenizer(List<AmazonFineFoodReview> partitionReviews) {
            this.partitionReviews = partitionReviews;
        }

        @Override
        public List<KV<String, Integer>> call() throws Exception {
            List<KV<String, Integer>> kv_pairs = new ArrayList<KV<String, Integer>>();

            for (AmazonFineFoodReview review : partitionReviews) {
                Pattern pattern = Pattern.compile("([a-zA-Z]+)");
                Matcher matcher = pattern.matcher(review.get_Summary());

                while (matcher.find())
                    kv_pairs.add(new KV(matcher.group().toLowerCase(), 1));
            }
            return kv_pairs;
        }
    }

    /**
     * the inner thread class to count the frequency of each unique word
     */
    private static class WordCount implements Callable<Map<String, Integer>> {

        List<KV<String, Integer>> words;

        public WordCount(List<KV<String, Integer>> words) {
            this.words = words;
        }

        @Override
        public Map<String, Integer> call() throws Exception {
            /* Count words */
            Map<String, Integer> results = new HashMap<>();

            for (KV<String, Integer> kv : words) {
                if (!results.containsKey(kv.getKey())) {
                    results.put(kv.getKey(), kv.getValue());
                } else {
                    int init_value = results.get(kv.getKey());
                    results.replace(kv.getKey(), init_value, init_value + kv.getValue());
                }
            }
            return new TreeMap<>(results);
        }
    }

    /**
     * merge all the sorted partitions
     *
     * @param old     the previous map to get the data from
     * @param current the new map to merge
     */
    public static void mergeMap(Map<String, Integer> old, Map<String, Integer> current) {
        if (current.isEmpty()) {
            current.putAll(old);
        } else {
            old.forEach((k, v) -> {
                if (current.containsKey(k)) {
                    current.put(k, v + current.get(k));
                } else {
                    current.put(k, v);
                }
            });
        }
    }

    /**
     * print the word count list/map
     *
     * @param wordcount
     */
    public static void print_word_count(Map<String, Integer> wordcount) {
        for (String word : wordcount.keySet()) {
            System.out.println(word + " : " + wordcount.get(word));
        }
    }

    public static void main(String[] args) {
        List<List<AmazonFineFoodReview>> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file);

        List<FutureTask<List<KV<String, Integer>>>> tokenizeFutureTasks = new ArrayList<>();
        Map<String, Integer> finalWordCount = new TreeMap<>();
        List<Map<String, Integer>> wordCounts = new ArrayList<>();

        //start timer for tokenize
        MyTimer myTokenizeTimer = new MyTimer("tokenizer");
        myTokenizeTimer.start_timer();
        //tokenize the words, each partitions to a thread
        //put tasks to a list for word count
        allReviews.forEach(r -> {
            FutureTask<List<KV<String, Integer>>> tokenizeTask = new FutureTask<>(new Tokenizer(r));
            new Thread(tokenizeTask).start();
            tokenizeFutureTasks.add(tokenizeTask);
        });
        myTokenizeTimer.stop_timer();

        //start timer for word count
        MyTimer myCountTimer = new MyTimer("wordCount");
        myCountTimer.start_timer();
        //wordCount each task being tokenize
        // put all counted word into a list
        tokenizeFutureTasks.forEach(t -> {
            try {
                List<KV<String, Integer>> words = t.get();
                FutureTask<Map<String, Integer>> countTask = new FutureTask<>(new WordCount(words));
                new Thread(countTask).start();
                wordCounts.add(countTask.get());
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.toString());
            }
        });
        myCountTimer.stop_timer();

        //merge all words at last
        wordCounts.forEach(c -> mergeMap(c, finalWordCount));

        print_word_count(finalWordCount);

        myTokenizeTimer.print_elapsed_time();
        myCountTimer.print_elapsed_time();

    }
}
